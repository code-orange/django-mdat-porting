# Generated by Django 3.1.7 on 2021-04-19 18:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("django_mdat_location", "0005_change_city_name_to_250_chars"),
        ("django_mdat_porting", "0002_adjust_ident_codes"),
    ]

    operations = [
        migrations.AlterField(
            model_name="mdatportingpartners",
            name="city",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_mdat_location.mdatcities",
            ),
        ),
        migrations.AlterField(
            model_name="mdatportingpartners",
            name="house_nr",
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name="mdatportingpartners",
            name="street",
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
