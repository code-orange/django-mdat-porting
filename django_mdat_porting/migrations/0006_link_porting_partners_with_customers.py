# Generated by Django 3.1.7 on 2021-04-19 21:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("django_mdat_customer", "0009_add_items_model"),
        ("django_mdat_porting", "0005_adjust_ident_codes"),
    ]

    operations = [
        migrations.CreateModel(
            name="MdatPortingPartnersToCustomers",
            fields=[
                ("id", models.BigAutoField(primary_key=True, serialize=False)),
                (
                    "customer",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="django_mdat_customer.mdatcustomers",
                    ),
                ),
                (
                    "porting_partner",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="django_mdat_porting.mdatportingpartners",
                    ),
                ),
            ],
            options={
                "db_table": "mdat_porting_partners_to_cust",
                "unique_together": {("porting_partner", "customer")},
            },
        ),
    ]
