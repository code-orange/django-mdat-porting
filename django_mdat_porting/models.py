from django_mdat_customer.django_mdat_customer.models import *
from django_mdat_location.django_mdat_location.models import *


class MdatPortingPartners(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=200)
    auth_reg_id = models.CharField(max_length=200)
    itu_carrier_code = models.CharField(max_length=20)
    city = models.ForeignKey(MdatCities, models.DO_NOTHING, null=True, blank=True)
    street = models.CharField(max_length=200, null=True, blank=True)
    house_nr = models.CharField(max_length=20, null=True, blank=True)

    class Meta:
        db_table = "mdat_porting_partners"
        unique_together = (("auth_reg_id", "itu_carrier_code"),)


class MdatPortingPartnersToCustomers(models.Model):
    id = models.BigAutoField(primary_key=True)
    porting_partner = models.ForeignKey(MdatPortingPartners, models.DO_NOTHING)
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING)
    view_customer = models.ForeignKey(
        MdatCustomers, models.DO_NOTHING, related_name="+"
    )

    class Meta:
        db_table = "mdat_porting_partners_to_cust"
        unique_together = (("porting_partner", "view_customer"),)
